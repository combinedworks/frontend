﻿using System;
using System.Collections.Generic;
using System.IO;  // this is for Stream reader
using System.Linq;
using System.Net; //this is for httpwebRequest
using System.Threading.Tasks;
using CredentialUserInterface.Models; //this is for credential model class
using Microsoft.AspNetCore.Http;  //for httpwebrequest
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json; //this is for Json convertor

namespace CredentialUserInterface.Controllers
{
    public class TransparentController : Controller
    {
        // GET: Transparent
        public ActionResult Index()
        {
            string url = "http://localhost:63599/Actions"; //after runing the user interface, it gives me this link. (actions refer to controller).
            try
            {
                List<CredentialModel> Credentials = new List<CredentialModel>(); //refering to credentialmodel class and say list. 
                string onReceive = "";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/showallrows"); //manual end point needs to go. refering to showallrows class to get the data from. 
                httpWebRequest.Method = "GET"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Result that we recieve from the API 200/400/500/
                int response = Convert.ToInt32(httpResponse.StatusCode); //convert to int
                if (response == 200) //if success then we will receive Jason
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        onReceive = streamReader.ReadToEnd();
                    }
                }
                Credentials = JsonConvert.DeserializeObject<List<CredentialModel>>(onReceive); //use Json converter to convert the list to Jason. it is also mapping. 
                return View(Credentials);
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }

        }

        // GET: Transparent/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Transparent/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Transparent/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Transparent/Edit/5
        public ActionResult Update(int id)
        {
            return View();
        }

        // POST: Transparent/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Transparent/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Transparent/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}