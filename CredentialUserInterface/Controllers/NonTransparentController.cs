﻿using System;
using System.Collections.Generic;
using System.IO; //this is for Streamreader
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CredentialUserInterface.Models; //for credential model class which set and get all data here in User Interface instead of showrow classes
using CredentialUserInterface.Models.Delete;
using CredentialUserInterface.Models.Insert;
using CredentialUserInterface.Models.Update;
using CredentialUserInterface.Models.ShowRow;
using Microsoft.AspNetCore.Http; //for httpwebrequest
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json; // for JsonConvert

namespace CredentialUserInterface.Controllers
{
    public class NonTransparentController : Controller //this class is the children of Controller (father)
    {
        string url = "http://localhost:63599/Actions"; //this link is the same at the server one which means this project is viewing what the server has from wrkbench. (actions refer to controller).

        // GET: NonTransparent
        public ActionResult Index() //this comes by defualt. Get method to retrieve data from workbench (through API). To create VIEW, right click on Index and add a list (check screenshot). 
        {
            
            try
            {
                List<CredentialModel> Credentials = new List<CredentialModel>(); //refering to credentialmodel class and say list. Credentials is local variable whcih is the name we gave it here. List<T> is ideal for storing and retrieving large number of elements. stores elements of the specified type and it grows automatically.
                string onReceive = "";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/showallrows"); //manual end point needs to go. refering to showallrows class to get the data from. 
                httpWebRequest.Method = "GET"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Result that we recieve from the API 200/400/500/
                int response = Convert.ToInt32(httpResponse.StatusCode); //define response and convert to int which said it is true 
                if (response == 200) //if success then we will receive Jason
                {
                    using var streamReader = new StreamReader(httpResponse.GetResponseStream()); //StreamReader is a helper class for reading characters from a Stream by converting bytes into characters using an encoded value. It can be used to read strings (characters) from different Streams like FileStream, MemoryStream, etc.
                    onReceive = streamReader.ReadToEnd(); //there was brackets here but the program simplified it for me. Use ReadToEnd method to read all the content from file, onReceive is to receive the data.
                }
                Credentials = JsonConvert.DeserializeObject<List<CredentialModel>>(onReceive); //use Json converter to convert the list to Jason. it is also mapping to credentialmodel class. 
                return View(Credentials);

            }
            catch (Exception err) // if any error then it goes here
            {
                return Content(err.ToString());
            }
        }
               
        // GET: NonTransparent/Details/5
        public ActionResult Details(int id) //this comes by defualt (Public ActionResult ....)
        {
            try
            {
                CredentialModel Credential = new CredentialModel
                {
                    CredentID = id
                };
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/showrow"); //manual end point needs to go
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(Credential));
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Where OK appears
                    Stream dataStream = httpResponse.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    string responseFromServer = "";
                    responseFromServer = reader.ReadToEnd();
                    CredentialModel returedRow = JsonConvert.DeserializeObject<CredentialModel>(responseFromServer);
                    return View(returedRow);

                }
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }
        }

        // GET: NonTransparent/Create
        public ActionResult Create() //the user will see the forms first in here and fill them, once he fills them it takes him to the second block of create (below)
        {
            return View();
        }

        // POST: NonTransparent/Create. 
        [HttpPost] //this comes by defualt
        [ValidateAntiForgeryToken] //this comes by defualt 
        public ActionResult Create(IFormCollection collection) //this comes by defualt
        {
            string timeStamp = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");  //fffz is Json timestamp
            InsertRequest insert = new InsertRequest(); //we call it from InsertRequest 
            try
            {
                insert = new InsertRequest() //insertrequest doesn't allow the user to insert the ID because we don't need them
                {
                    UserName = collection["UserName"].ToString(),  //collection can be anything like var, it is a parameter like ID which accepts anything from the form.
                    EncryptPW = collection["EncryptPW"].ToString(),
                    //EKey = collection["EKey"].ToString(),  
                    WebsiteName = collection["WebsiteName"].ToString(),
                    WebsiteURL = collection["WebsiteURL"].ToString(),
                    //TStamp = timeStamp,   //not required to fill in
                    //SelectedServer = collection["SelectedServer"].ToString()   //not required to fill in
                };
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/insert"); //manual end point needs to go
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(insert));
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Where OK appears
                    Stream dataStream = httpResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);   // Open the stream using a StreamReader for easy access.  
                    string responseFromServer = "";   // Read the content.  
                    responseFromServer = reader.ReadToEnd();
                    GeneralResponse returedRow = JsonConvert.DeserializeObject<GeneralResponse>(responseFromServer);
                    if (returedRow.ResultCode == 200)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        return Content(returedRow.ResultDescription);
                    }
                }
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }
        }

        // GET: NonTransparent/Edit/5
        public ActionResult Update(int id) //this comes by defualt (Public ActionResult ....)
        {
            try  //we used the same codes as Details in this block
            {
                CredentialModel Credential = new CredentialModel
                {
                    CredentID = id
                };
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/showrow"); //manual end point needs to go
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(Credential));
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Where OK appears
                    Stream dataStream = httpResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);   // Open the stream using a StreamReader for easy access.  
                    string responseFromServer = "";
                    responseFromServer = reader.ReadToEnd(); //read the content and convert it to Json in Credential model class
                    CredentialModel returedRow = JsonConvert.DeserializeObject<CredentialModel>(responseFromServer);
                    return View(returedRow);

                }
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }
        }

        // POST: NonTransparent/Edit/5
        [HttpPost]  //this comes by defualt
        [ValidateAntiForgeryToken] //this comes by defualt
        public ActionResult Update(int id, IFormCollection collection) //this comes by defualt
        {
            string rawPassword = collection["EncryptPW"].ToString();
            //encyption
            //string encryptedPassword = rawPassword;
            //string createdKey = "NoKey";
            //
            string timeStamp = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            UpdateRequest insert = new UpdateRequest();
            try
            {
                insert = new UpdateRequest()
                {
                    CredentID = id,
                    UserName = collection["UserName"].ToString(),
                    EncryptPW = rawPassword,
                    //EKey = createdKey,
                    WebsiteName = collection["WebsiteName"].ToString(),
                    WebsiteURL = collection["WebsiteURL"].ToString(),
                    //TStamp = timeStamp,
                    //SelectedServer = "NonT"
                };
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/update"); //manual end point needs to go
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(insert));
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Where OK appears
                    Stream dataStream = httpResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);    // Open the stream using a StreamReader for easy access.  
                    string responseFromServer = "";                      // Read the content.  
                    responseFromServer = reader.ReadToEnd();
                    GeneralResponse returedRow = JsonConvert.DeserializeObject<GeneralResponse>(responseFromServer);
                    if (returedRow.ResultCode == 200)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        return Content(returedRow.ResultDescription);
                    }


                }
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }
        }

        // GET: NonTransparent/Delete/5
        public ActionResult Delete(int id) //this comes by defualt
        {
            try  //we used the same codes as Details in this block
            {
                CredentialModel Credential = new CredentialModel
                {
                    CredentID = id
                };
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/showrow"); //manual end point needs to go
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST"; //it is a get request
                httpWebRequest.Accept = "application/json"; //Accept Json in return
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(Credential));
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Where OK appears
                    Stream dataStream = httpResponse.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    string responseFromServer = "";
                    responseFromServer = reader.ReadToEnd();
                    CredentialModel returedRow = JsonConvert.DeserializeObject<CredentialModel>(responseFromServer);
                    return View(returedRow);

                }
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }
        }

        // POST: NonTransparent/Delete/5
        [HttpPost] //this comes by defualt
        [ValidateAntiForgeryToken] //this comes by defualt
        public ActionResult Delete(int id, IFormCollection collection) //this comes by defualt
        {
            try
            {
                DeleteRequest delete = new DeleteRequest()  //find ID of an account and delete it from database
                {
                    CredentID = id,
                };
                
                var httpWebRequest = (HttpWebRequest)WebRequest.Create($"{url}/delete");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Accept = "application/json";
                var returnResult = new CredentialModel();
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(JsonConvert.SerializeObject(delete));
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse(); // Where OK appears
                    Stream dataStream = httpResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);  // Open the stream using a StreamReader for easy access. 
                    string responseFromServer = "";  // Read the content.
                    responseFromServer = reader.ReadToEnd();
                    returnResult = JsonConvert.DeserializeObject<CredentialModel>(responseFromServer);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception err) // if any error goes here
            {
                return Content(err.ToString());
            }

        }
    }
}