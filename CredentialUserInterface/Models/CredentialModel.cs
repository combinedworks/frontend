﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CredentialUserInterface.Models
{
    public class CredentialModel //this class is linked with Controller (both) as USING and controllers call it to return the list of data (through DTO) and then from workbench. 
    {
        public int CredentID { get; set; } //the user needs to identfy a row using ID to delete a row so it is required.
        public string UserName { get; set; } = string.Empty;
        public string EncryptPW { get; set; } = string.Empty;
        public string EKey { get; set; } = string.Empty;
        public string WebsiteName { get; set; } = string.Empty;
        public string WebsiteURL { get; set; } = string.Empty;
        public string TStamp { get; set; } = string.Empty;
        public string SelectedServer { get; set; } = string.Empty; //this is for selected server
    }
}
